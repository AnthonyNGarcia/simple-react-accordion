import './App.css';
import AccordionComponent from './AccordionComponent';

function App() {
  return (
    <div className="App">
      <AccordionComponent sampleText = "Open Me">

        <AccordionComponent sampleText = "AA">
          <AccordionComponent sampleText = "aa1"/>
          <AccordionComponent sampleText = "aa2"/>
        </AccordionComponent>

        <AccordionComponent sampleText = "BB">
          <AccordionComponent sampleText = "bb1"/>
          <AccordionComponent sampleText = "bb2"/>
        </AccordionComponent>

        <AccordionComponent sampleText = "CC">
          <AccordionComponent sampleText = "cc1"/>
          <AccordionComponent sampleText = "cc2"/>
        </AccordionComponent>

      </AccordionComponent>
    </div>
  );
}

export default App;
