import React, { useState } from 'react';
import './App.css';

function AccordionComponent(props) {

    const [showChildren, setShowChildren] = useState(false);

    const toggleChildrenHandler = () => {
        setShowChildren(!showChildren);
    }

    return (
        <div className  = {"accordion " + (showChildren ? "active" : "inactive")} >
            <h2 className = {"accordion-text " + (showChildren ? "active" : "inactive")} onClick = {toggleChildrenHandler}>{props.sampleText}</h2>
            {showChildren ? props.children : null}
            <p>I am accordion things.</p>
        </div>
    )
}

export default AccordionComponent;